﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public static Spawner instance;

    public GameObject[] prefabs;
    public int poolSize = 20;
    GameObject[][] pools;
    int[] currentIndex;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        pools = new GameObject[prefabs.Length][];
        currentIndex = new int[prefabs.Length];
        for (int j = 0; j < prefabs.Length; j++)
        {
            pools[j] = new GameObject[poolSize];
            for (int i = 0; i < poolSize; i++)
            {
                GameObject g = Instantiate(prefabs[j]);
                gameObject.SetActive(false);
                pools[j][i] = g;
            }

        }
    }
    public GameObject Spawn(string name)
    {
        for (int i = 0; i < prefabs.Length; i++)
        {
            GameObject prefab = prefabs[i];
            if (prefab.name == name)
            {
                int current = currentIndex[i];
                GameObject[] pool = pools[i];
                GameObject g = pool[current];
                g.SetActive(true);
                current++;
                current %= poolSize;
                return g;
            }
        }
        return null;
    }
}
