﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllRangeStart : MonoBehaviour {

    public static bool AllRange = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.name != "bullet")
        {
            AllRange = true;
        }
    }
}
