﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner2 : MonoBehaviour {

    public static Spawner2 instance;

    public GameObject prefab;
    private List<GameObject> pool = new List<GameObject>();


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public GameObject Spawn(string name)
    {
        GameObject g = pool.Find(IsInactive);
        if (g == null)
        {
            g = Instantiate(prefab);
            pool.Add(g);
        }
        g.SetActive(true);
        return g;
    }
    bool IsInactive (GameObject g)
    {
        return g.activeSelf;
    }
}