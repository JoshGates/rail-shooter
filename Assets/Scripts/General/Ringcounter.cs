﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ringcounter : MonoBehaviour {

    public static int rings = 0;
    private int rings2 = 0;
    public AudioSource bwip;

    void Update () {
		if (rings != rings2)
        {
            bwip.pitch = Random.Range(0.9f, 1.1f);
            bwip.Play();
            rings2 = rings;
        }
	}
}
