﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateTrigger : MonoBehaviour {

    bool triggered = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && triggered==false)
        {
            Ringcounter.rings++;
            triggered = true;
            this.gameObject.SetActive(false);
        }
    }
}