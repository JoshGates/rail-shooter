﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public AudioSource introSong;
    public AudioSource mainSong;
    public static bool IsPlaying = false;
    public Camera cam1;
    public Camera cam2;
 
    void Start()
    {
        cam1.enabled = false;
        cam2.enabled = true;
        StartCoroutine(PlayMusic());
    }

    void Update () {
		if (Input.GetKeyDown("space") && IsPlaying == false)
        {
            IsPlaying = true;
            cam2.enabled = false;
            cam1.enabled = true;
        }
	}
    IEnumerator PlayMusic()
    {
        yield return new WaitForSeconds(1);
        
        while (enabled)
        {
            while (!IsPlaying && !introSong.isPlaying && ShipAnims.health>=1) { 
            introSong.Play();
            }

            if (!introSong.isPlaying && !mainSong.isPlaying && IsPlaying)
            {
                mainSong.Play();
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
