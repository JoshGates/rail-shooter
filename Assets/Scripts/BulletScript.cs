﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    private Vector3 T;

    private void OnEnable()
    {
        StartCoroutine(KillSelf());

    }

    IEnumerator KillSelf()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        if (other.transform.parent != null && other.transform.parent.name == "Exploder")
        {

            other.gameObject.SetActive(false);
        }
        else if (other.name == "Eship 1")
        {
            other.gameObject.SetActive(false);
        }
    }
}
