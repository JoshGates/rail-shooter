﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingBullet : MonoBehaviour {

    public GameObject player;
    Rigidbody body;
    public float speed = 20;

    void Update () {
    if (Vector3.Distance(this.transform.position, player.transform.position) < 400)
        {
            MoveTowards();
        }
    }
    void MoveTowards()
        {
            Vector3 diff = player.transform.position - transform.position;
            body.velocity = diff.normalized * speed;
        }
}
