﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    private Vector3 T;

    private void OnEnable()
    {
        StartCoroutine(KillSelf());

    }

    IEnumerator KillSelf()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("hit");
            Debug.Log(ShipAnims.health);
            ShipAnims.health--;
            Debug.Log(ShipAnims.health);
        }
    }
}
