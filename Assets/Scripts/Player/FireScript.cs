﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour {

    private bool firing;
    private bool firingMode;
    public float speed = 100;
    private Rigidbody rb;
    private GameObject g;
    public GameObject lazer;
    public GameObject Homing;

    void Start()
    {
        StartCoroutine(Shoot1());
    }

    IEnumerator Shoot1()
    {
       while (enabled) { 
            if (Input.GetKey("space") && !firingMode)
            {
                GameObject g = Instantiate(lazer);
                Vector3 v = this.transform.position;
                v.z = v.z + 1;
                g.transform.position = v;
                Rigidbody rb = g.GetComponent<Rigidbody>();
                rb.velocity = speed * Vector3.forward*2;
                yield return new WaitForSeconds(.05f);
            }
            yield return null;
        }
    }
    IEnumerator Shoot2()
    {
        while (enabled)
        {
            if (Input.GetKey("space") && firingMode)
            {
                GameObject g = Instantiate(Homing);
                Vector3 v = this.transform.position;
                v.z = v.z + 1;
                g.transform.position = v;
                Rigidbody rb = g.GetComponent<Rigidbody>();
                rb.velocity = speed * Vector3.forward * 2;
                yield return new WaitForSeconds(.05f);
            }
            yield return null;
        }
    }
}