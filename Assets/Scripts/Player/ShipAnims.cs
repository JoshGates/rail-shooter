﻿using UnityEngine;

public class ShipAnims : MonoBehaviour {

    public static ShipAnims instance;

    public static bool dead = false;
    public float speed = 10;
    public float forward = 2;
    public static float health = 30;

    Animator anim;
    public Rigidbody body;
    public Camera cam;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start ()
    {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
	}
	
	void Update ()
    {
        if (GameManager.IsPlaying == true && !AllRangeStart.AllRange)
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            anim.SetFloat("rightVelocity", x);
            anim.SetFloat("upVelocity", -y);
        
            body.velocity = new Vector3(x, -y, forward) * speed;
        }

        if (AllRangeStart.AllRange)
        {
            forward = 85;
            transform.position += transform.forward * Time.deltaTime * forward;
            float x = Input.GetAxisRaw("Horizontal");
            float z = Input.GetAxisRaw("Vertical");
            transform.Rotate(Vector3.right * z * Time.deltaTime * speed);
            transform.Rotate(Vector3.up * x * Time.deltaTime * speed);
        }

        if (health <= 0)
        {
                cam.gameObject.transform.parent = null;
                body.velocity = new Vector3(0,0,0);
                ShipAnims.instance.gameObject.SetActive(false);
        }     
    }
}