﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour {

    private bool firing;
    private GameObject g;
    private Rigidbody rb;
    public float speed = 100;
    public GameObject lazer;
    public GameObject ship;
    public float fireSpeed = .05f;
    public AudioSource fire;


    void Start()
    {
        StartCoroutine(Shoot());
    }

    IEnumerator Shoot()
    {
       while (enabled) { 
            if (Input.GetKey("space"))
            {
                GameObject g = Instantiate(lazer);
                //g.transform.SetParent(this.transform);
                Vector3 v = this.transform.position;
                v += this.transform.forward;
                g.transform.position = v;
                g.transform.rotation = ship.transform.rotation;
                Rigidbody rb = g.GetComponent<Rigidbody>();
                rb.velocity = speed * this.transform.forward;
                yield return new WaitForSeconds(fireSpeed);
                fire.Play();

            }
            yield return null;
        }
    }
}