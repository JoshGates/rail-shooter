﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public Image healthbar;

    public float healthRatio;

	void Update () {

        healthRatio = ShipAnims.health / 30;

        healthbar.fillAmount = healthRatio;
	}
}
