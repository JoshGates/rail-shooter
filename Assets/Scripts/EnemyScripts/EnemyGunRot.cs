﻿using System.Collections;
using UnityEngine;

public class EnemyGunRot : MonoBehaviour {

    bool hasStarted;

    public float speed = 100;

    public AudioSource shoot;
    public GameObject Barrel;
    public GameObject Bullet;
    private GameObject g;


    void OnEnable()
    {
        hasStarted = false;
    }

    void Update() {

        Vector3 dir = ShipAnims.instance.transform.position - this.transform.position;

        if (Vector3.Dot(dir, this.transform.up) < 0 && Vector3.Distance(this.transform.position, ShipAnims.instance.transform.position) < 200)
        {
            transform.LookAt(ShipAnims.instance.transform.position + ShipAnims.instance.body.velocity * 0.333f);

            if (!hasStarted)
            {
                hasStarted = true;
                StartCoroutine(FiringDelay());
            }
        }
    }

    // leads the player and fires bursts of bullets at a timed delay
    IEnumerator FiringDelay()
    {
        while (enabled)
        {
            if (Vector3.Distance(this.transform.position, ShipAnims.instance.transform.position) < 200)
            {
                yield return new WaitForSeconds(.4f);

                shoot.Play();

                for (int i = 0; i < 6; i++)
                {
                    yield return new WaitForSeconds(.1f);
                    GameObject g = Instantiate(Bullet);
                    Rigidbody rb = g.GetComponent<Rigidbody>();
                    g.transform.rotation = Barrel.transform.rotation;
                    g.transform.position = Barrel.transform.position;
                    rb.velocity = speed * Barrel.transform.forward;
                }

                hasStarted = false;
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
