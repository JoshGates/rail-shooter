﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploderScript : MonoBehaviour
{
    public Animator anim;
    public GameObject player;
    Rigidbody body;
    public float speed = 20;
    bool exploding = false;

    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Vector3.Distance(this.transform.position, player.transform.position) < 55)
        {
            StartCoroutine(Explode());
        }
        else if (Vector3.Distance(this.transform.position, player.transform.position) < 400)
        {
            MoveTowards();
        }
       
    }
    void MoveTowards()
    {
        Vector3 diff = player.transform.position - transform.position;
        body.velocity = diff.normalized * speed;
    }
    IEnumerator Explode()
    {
        if (!exploding)
        {
            exploding = true;
            anim.SetTrigger("InRange");
            body.velocity = Vector3.zero;
            yield return new WaitForSeconds(1.3f);
            this.gameObject.SetActive(false);
        }
    }

    public void Splode ()
    {
        StartCoroutine(Explode());
    }
}