﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public static bool dead = false;
    public float speed = 10;
    public float forward = 2;
    public float y;
    public float x;
    public float yDiff;

    Animator anim;
    Rigidbody body;

    void Start ()
    {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        //StartCoroutine(Movement());
    }
	
	void Update ()
    {
        Vector3 diff = ShipAnims.instance.transform.position - this.transform.position;
        x = Mathf.Clamp(diff.x, -.2f, .2f);
        yDiff = diff.y - this.transform.position.y;
        if (ShipAnims.instance.transform.position.y > this.transform.position.y)
        {
            y = 1;
        }
        else if (yDiff<-60)
        {
            y = -1;
        }
        else
        {
            y = 0;
        }
        
        //anim.SetFloat("rightVelocity", x);
        //anim.SetFloat("upVelocity", -y);

        body.velocity = new Vector3(x, y, -forward) * speed;
    }

    /*IEnumerator Movement()
    {
        while (enabled)
        {
                
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }*/
}
