﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag ("Player"))
        {
            
            GameManager.IsPlaying = false;
            ShipAnims.health -= 5;
        }
        else if (other.transform.parent != null && other.transform.parent.name == "Exploder")
        {
                ExploderScript explosion = other.GetComponent<ExploderScript>();
                explosion.Splode();
        }
    }
}
