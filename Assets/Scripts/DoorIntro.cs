﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorIntro : MonoBehaviour {

    public GameObject ships;
    public Animator door;

    void OnTriggerEnter(Collider other)
    {
        Vector3 stop = new Vector3(0.9429408f, 3.890291f, 7486.7f);
        if (other.CompareTag("Player"))
        {
            for (float i = 0; i < 4; i += Time.deltaTime)
            {
                GameManager.IsPlaying = false;
                ShipAnims.instance.body.velocity = new Vector3(0, 0, 0);
                ShipAnims.instance.transform.position = Vector3.Lerp(ShipAnims.instance.transform.position, stop, i);
            }
            StartCoroutine(Begin());
        }
        
    }
    IEnumerator Begin()
    {
        door.SetTrigger("Open");
        yield return new WaitForSeconds(4);
        ships.SetActive(true);
        GameManager.IsPlaying = true;
    }
}
